#!/bin/sh
# wait-for-synapse.sh
#
# based on an exmaple from https://docs.docker.com/compose/startup-order/
#
# command: ["/usr/src/app/wait-for-synapse.sh", "http://synapse:8008", "python", "/usr/src/app/pnut-matrix.py", "-d"]

set -e

host="$1"
# Shift arguments with mapping:
# - $0 => $0
# - $1 => <discarded>
# - $2 => $1
# - $3 => $2
# - ...
# This is done for `exec "$@"` below to work correctly
shift

until [ "200" -eq $(curl -s -o /dev/null --head -w "%{http_code}" ${host}/_matrix/client/versions) ]; do
    >&2 echo "synapse is unavailable - sleeping"
    sleep 3
done

>&2 echo "synapse is up - executing command"
exec "$@"
