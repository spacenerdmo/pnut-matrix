# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Docker image

### Fixed
- Self-invite when sending a PM from a pnut app

## [1.3.0] - 2022-08-20
### Added
- Support for matrix stickers

### Fixed
- External matrix users unable to DM appservice
- Joining a non-public channels on pnut makes matrix room public
- Timestamps on generated events

### Changed
- Improved display name for pnut users in matrix rooms
- Access tokens redacted from appservice logs

## [1.2.0] - 2021-03-20
### Added
- Support of private messaging and private channels

## [1.1.1] - 2021-03-05
### Fixed
- Pass correct image size info for image attachments

## [1.1.0] - 2021-02-27
### Added
- Support for pnut v1 app streams

## [1.0.2] - 2019-02-02
### Fixed
- error checking when avatar changes
- hung thread when websocket is closed remotely
- error checking for image size
- error handling message redaction from moderator

## [1.0.1] - 2019-01-09
### Fixed
- mentions from pnut to matrix
- startup for pnut-matrix-bot

## [1.0.0] - 2019-01-03
### Fixed
- database initialization

### Added
- Support for pnut app streams
- Sync avatars from pnut to matrix
- Administrator control room functions
- Example configuration file

### Changed
- Display names for matrix users not registered with pnut
- Simplified storage models

## 0.0.1 - 2018-08-23
### Added
- This CHANGELOG file because I can't believe for the last year I wasn't
keeping track of releases for this project. :p

[Unreleased]: https://gitlab.dreamfall.space/thrrgilag/pnut-matrix/compare/1.0.2...HEAD
[1.0.2]: https://gitlab.dreamfall.space/thrrgilag/pnut-matrix/tags/1.0.2
[1.0.1]: https://gitlab.dreamfall.space/thrrgilag/pnut-matrix/tags/1.0.1
[1.0.0]: https://gitlab.dreamfall.space/thrrgilag/pnut-matrix/tags/1.0.0
[0.0.1]: https://gitlab.dreamfall.space/thrrgilag/pnut-matrix/tags/v0.0.1
