from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import yaml
import os

configyaml = os.environ.get("CONFIG_FILE")

with open(configyaml, "rb") as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)

engine = create_engine(config['SERVICE_DB'])
db_session = scoped_session(sessionmaker(bind=engine))

Base = declarative_base()

Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import models
    Base.metadata.create_all(bind=engine)
