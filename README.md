# The Matrix <--> Pnut bridge

This is a pnut.io channel bridge for Matrix using the Application Services (AS) API.

This bridge will pass pnut.io channel messages through to Matrix, and Matrix messages through to pnut.io channels.


## Usage

The public bridge is once again online!

See [Using-the-public-bridge](https://gitlab.com/thrrgilag/pnut-matrix/-/wikis/Using-the-public-bridge) for details.


## Installation

Currently pnut-matrix has been only tested with and confirmed to work with [synapse]. Please refer to the [synapse installation instructions] for details on how to setup your homeserver.

To install the latest version of pnut-matrix from source:

```sh
git clone https://gitlab.com/thrrgilag/pnut-matrix.git
cd pnut-matrix
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```


## Configuration

Copy `config.yaml-sample` to `config.yaml` and edit for your setup. Likewise copy `appservice.yaml-sample` to `appservice.yaml` and edit to match the tokens, prefix, and port listed in `config.yaml`. This is the configuration that synapse will need to reference so that it can connect the bridge. Make sure you modify your [syanpse configuration] accordingly.


## Tweaks

There exists a bug in synapse which prevents the bridge user from accepting DM invites from users on other homeservers. To work around the issue you can create a profile for the bot user.[^1]

```sh
curl --data '{"type": "m.login.application_service", "username": "your_sender_localpart"}' 'http://yourhomeserver/_matrix/client/r0/register?access_token=your_as_token'
```


## Contributing and support

You can open issues for bugs or feature requests and you can submit merge requests to this project on [GitLab]. You can also submit issues and patches directly to [morgan@mcmillian.dev].

Join my public chat room for development discussion.

- [pnut-matrix on pnut.io]
- [#pnut_999:pnut-matrix.dreamfall.space]


## License

GPLv3, see [LICENSE].

[synapse]: https://github.com/matrix-org/synapse
[synapse installation instructions]: https://matrix-org.github.io/synapse/latest/setup/installation.html
[syanpse configuration]: https://matrix-org.github.io/synapse/latest/application_services.html
[GitLab]: https://gitlab.com/thrrgilag/pnut-matrix/
[morgan@mcmillian.dev]: mailto:morgan@mcmillian.dev
[pnut-matrix on pnut.io]: https://patter.chat/999
[#pnut_999:pnut-matrix.dreamfall.space]: https://matrix.to/#/#pnut_999:pnut-matrix.dreamfall.space
[LICENSE]: LICENSE
[^1]: https://github.com/matrix-org/matrix-appservice-irc/issues/1270#issuecomment-849765090
